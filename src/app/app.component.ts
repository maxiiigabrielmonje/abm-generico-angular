import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ArticulosService } from './articulos.service';
import { FormGroup } from '@angular/forms';
29.
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  articulos: any;
  articulo: FormGroup;


  art = {
    codigo: 0,
    descripcion: "",
    precio: 0
  }

  constructor(private articulosServicio: ArticulosService, private fb: FormBuilder) {
    this.articulos = this.articulosServicio.recuperarTodos();
    this.articulo = this.fb.group({
      descripcion: this.fb.control('', [Validators.required]),
      precio: this.fb.control('', [Validators.required])
    });
  }

  ngOnInit() {
    this.recuperarTodos();
    this.articulo = this.fb.group({
      descripcion: this.fb.control('', [Validators.required]),
      precio: this.fb.control('', [Validators.required, Validators.min(1)])
    });
  }

  recuperarTodos() {
    this.articulosServicio.recuperarTodos().subscribe((result: any) => this.articulos = result);
  }

  alta() {
    this.articulosServicio.alta(this.art).subscribe((datos: any) => {
      if (datos['resultado'] == 'OK') {
        alert(datos['mensaje']);
        this.recuperarTodos();
      }
    });
  }

  baja(codigo: number) {
    this.articulosServicio.baja(codigo).subscribe((datos: any) => {
      if (datos['resultado'] == 'OK') {
        alert(datos['mensaje']);
        this.recuperarTodos();
      }
    });
  }

  modificacion() {
    this.articulosServicio.modificacion(this.art).subscribe((datos: any) => {
      if (datos['resultado'] == 'OK') {
        alert(datos['mensaje']);
        this.recuperarTodos();
      }
    });
  }

  seleccionar(codigo: number) {
    this.articulosServicio.seleccionar(codigo).subscribe((result: any) => this.art = result[0]);
  }

  hayRegistros() {
    return true;
  }
}
